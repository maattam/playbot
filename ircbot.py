from ircconnection import IrcConnection

class IrcBot:

    # Settings: dictionary of bot settings:
    #   nick:       'thebot'
    #   user:       'botuser'
    #   realname:   'Erkki Botti'
    #   channel:    'defaultchannel'

    def __init__(self, settings):
        self.settings = settings
        self.irc = IrcConnection()

        # Register join handler
        self.irc.handle('001', HandleJoin(settings['channel']))

    def debug(self, flag):
        self.irc.debug(flag)

    def connect(self, host, port):
        self.irc.connect(host, port)

        # If password is set, send it first
        if 'password' in self.settings:
            self.irc.send('PASS %s' % self.settings['password'])
        
        # Send nick and user
        self.irc.send('NICK %s' % self.settings['nick'])
        self.irc.send('USER %s a a :%s' % (self.settings['user'], self.settings['realname']))

    def loop(self):
        while self.irc.good():
            self.irc.recv()

    # handlers: list of tuples ('PRIVMSG', IrcHandler())
    def registerHandlers(self, handlers):
        for handler in handlers:
            self.irc.handle(handler[0], handler[1])

class HandleJoin:
    
    def __init__(self, channel):
        self.channel = channel

    def main(self, irc, args, message):
        irc.send('JOIN %s' % self.channel)
