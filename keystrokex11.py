from subprocess import call


# Keystroke automation using xautomation package
class KeystrokeX11:

    # TODO: Specify window handle?
    def __init__(self, keys):
        self.keys = keys


    def main(self, irc, args, message):
        command = message.lower()
        nick = args[0][:args[0].find('!')]

        if command in self.keys:
            key = self.keys[command]
            
            print('%-15s %5s' % (nick, command))
            
            # Send keystroke with delay
            call(['xte',
                'keydown %s' % key[0],
                'sleep %d' % key[1],
                'keyup %s' % key[0]
            ])
