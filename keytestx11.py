from keystrokex11 import KeystrokeX11
import time


# Key mappings
keys = {
    'up':       ('Up',      0.25),
    'left':     ('Left',    0.25),
    'down':     ('Down',    0.25),
    'right':    ('Right',   0.25),
    'a':        ('V',      0.01),
    'b':        ('C',      0.01),
    'x':        ('X',      0.01),
    'y':        ('D',      0.01),
    'start':    ('N',      0.01),
    'select':   ('M',      0.01)
}

key = KeystrokeX11(keys)

time.sleep(5)
key.main(None, ['kayraerajormaaa!'], 'a')
key.main(None, ['kayraerajormaaa!'], 'b')
key.main(None, ['kayraerajormaaa!'], 'down')
