from keystroke import Keystroke
import win32con as wc
import time

# Key mappings
keys = {
    'up':       (wc.VK_UP,      0.25),
    'left':     (wc.VK_LEFT,    0.25),
    'down':     (wc.VK_DOWN,    0.25),
    'right':    (wc.VK_RIGHT,   0.25),
    'a':        (ord('V'),      0.01),
    'b':        (ord('C'),      0.01),
    'x':        (ord('X'),      0.01),
    'y':        (ord('D'),      0.01),
    'start':    (ord('N'),      0.01),
    'select':   (ord('M'),      0.01)
}

key = Keystroke(keys,)

time.sleep(5)
key.main(None, ['kayraerajormaaa!'], 'a')
key.main(None, ['kayraerajormaaa!'], 'b')
key.main(None, ['kayraerajormaaa!'], 'down')