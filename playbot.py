from ircbot import IrcBot
try:
    import win32con as wc
    from keystroke import Keystroke
    platform = "Windows"
except ImportError:
    from keystrokex11 import KeystrokeX11 as Keystroke
    platform = "Linux"

settings = {
    'nick':     'pelibotti',
    'user':     'pelibotti',
    #'password': '',
    'realname': 'Twitch bot, made by lese',
    'channel':  '#twitchtest'
}

# Key mappings
if(platform == "Windows"):
    keys = {
        'up':       (wc.VK_UP,      0.3),
        'left':     (wc.VK_LEFT,    0.3),
        'down':     (wc.VK_DOWN,    0.3),
        'right':    (wc.VK_RIGHT,   0.3),
        'a':        (ord('V'),      0.025),
        'b':        (ord('C'),      0.025),
        'x':        (ord('X'),      0.025),
        'y':        (ord('D'),      0.025),
        'start':    (ord('N'),      0.025),
        'select':   (ord('M'),      0.025)
    }
else:
    keys = {
        'up':       ('Up',      0.3),
        'left':     ('Left',    0.3),
        'down':     ('Down',    0.3),
        'right':    ('Right',   0.3),
        'a':        ('V',      0.025),
        'b':        ('C',      0.025),
        'x':        ('X',      0.025),
        'y':        ('D',      0.025),
        'start':    ('N',      0.025),
        'select':   ('M',      0.025)
    }

handlers = [('PRIVMSG', Keystroke(keys))]

bot = IrcBot(settings)
bot.registerHandlers(handlers)
#bot.debug(True)

bot.connect('irc.cc.tut.fi', 6667)
bot.loop()
