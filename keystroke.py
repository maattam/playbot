import win32api
import win32gui
import win32con as wc
import time

class Keystroke:

    # If no window name is specified, keybd_event is used globally
    def __init__(self, keys, window = None):
        self.keys = keys
        self.handle = 0

        # Find window
        if window != None:
            self.handle = win32gui.FindWindow(None, window)
            if self.handle == 0:
                print('Window not found: ' + window)

    def main(self, irc, args, message):
        command = message.lower()
        nick = args[0][:args[0].find('!')]

        if command in self.keys:
            key = self.keys[command]

            print('%-15s %5s' % (nick, command))

            if self.handle != 0:
                win32api.PostMessage(self.handle, wc.WM_KEYDOWN, key[0], 0)
                time.sleep(key[1])
                win32api.PostMessage(self.handle, wc.WM_KEYUP, key[0], 0)
            
            else:
                win32api.keybd_event(key[0], 0x9e, 0, 0)
                time.sleep(key[1])
                win32api.keybd_event(key[0], 0x9e, wc.KEYEVENTF_KEYUP, 0)