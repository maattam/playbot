import socket

class IrcConnection:

    def __init__(self):
        self.done = False
        self.verbose = False

        # Irc message buffering
        self.buffer = ''

        self.handlers = {}

        # Create socket for irc connection
        self.socket = socket.socket()

    def debug(self, flag):
        self.verbose = flag

    def connect(self, host, port):
        self.socket.connect((host, port))

    def handle(self, command, handler):
        if command in self.handlers:
            self.handlers[command].append(handler)
        else:
            self.handlers[command] = [handler]

    def send(self, line):
        if self.verbose:
            print('< ' + line)

        self.socket.send(line + '\r\n')

    def quit(self, message):
        self.socket.send('QUIT :%s' % message)
        self.done = True

    def good(self):
        return not self.done

    # Reads one line
    def recv(self):
        self.buffer += self.socket.recv(512)

        # Find delimiter
        index = self.buffer.find('\r\n')
        while index != -1:
            line = self.buffer[:index]

            # Remove line from buffer
            self.buffer = self.buffer[index+2:]

            self.__handleLine(line)
            
            # Fetch next command
            index = self.buffer.find('\r\n')

    def __handleLine(self, line):
        if self.verbose:
            print('> ' + line)

        args = []
        message = ''

        # Strip first : if exists
        if line[0] == ':':
            line = line[1:]

        # Find index where payload data begins
        payload = line.find(':')

        # Don't split payload
        if payload != -1:
            args = line[:payload].split(' ')
            message = line[payload+1:]

        else:
            args = line.split(' ')

        # Sanity check
        if len(args) == 0:
            return

        # Respond to ping
        if args[0] == 'PING':
            self.send('PONG :%s' % message)

        # Process message handlers
        self.__check(0, args, message)
        self.__check(1, args, message)

    def __check(self, index, args, message):
        if index >= len(args):
            return

        if args[index] in self.handlers:
            for handler in self.handlers[args[index]]:
                handler.main(self, args, message)
